package com.example.springcloudstreamconsumer;

import java.util.function.Consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.springcloudstreamconsumer.models.Person;

@SpringBootApplication
public class SpringCloudStreamConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamConsumerApplication.class, args);
	}

	@Bean
	public Consumer<Person> display() {
		return person -> {
			System.out.println("Consumed: " + person.toString());
		};
	}
}
