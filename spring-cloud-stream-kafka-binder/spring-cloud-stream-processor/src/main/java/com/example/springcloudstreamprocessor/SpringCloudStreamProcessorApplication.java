package com.example.springcloudstreamprocessor;

import java.util.function.Function;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.springcloudstreamprocessor.models.Person;

@SpringBootApplication
public class SpringCloudStreamProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamProcessorApplication.class, args);
	}
	
	@Bean
	public Function<Person, Person> process() {
		return person -> person;
	}

}
