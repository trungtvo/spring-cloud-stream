package com.example.springcloudstreampublisher;

import java.util.Random;
import java.util.function.Supplier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.springcloudstreampublisher.models.Person;

@SpringBootApplication
public class SpringCloudStreamPublisherApplication {
	private String[] users = {"Trung Vo", "HongPhuong Chu", "Ron", "Aiko", "Roko"};

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamPublisherApplication.class, args);
	}
	
	@Bean
	public Supplier<Person> person() {
		return () -> {
			int idx = new Random().nextInt(5);
			Person person = new Person(idx, this.users[idx]);
			System.out.println("Publish: " + person.toString());
			return person;
		};
	}

}
